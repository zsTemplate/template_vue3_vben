import type { App } from 'vue';

// import createZhanli from '/@/components/GlobFunction/createZhanli';
// import createCamp from '/@/components/GlobFunction/createCamp';
// import getCamp from '/@/components/GlobFunction/getCamp';
// import getWeapons from '/@/components/GlobFunction/getWeapons';
// import getWeaponDetails from '/@/components/GlobFunction/getWeaponDetails';
// import getCampDetails from '/@/components/GlobFunction/getCampDetails';

// 注册全局：函数和变量
export function registerGlobProperties(app: App) {
  // // 创建战例
  // app.config.globalProperties.$createZhanli = createZhanli;

  // // 创建 作战编成
  // app.config.globalProperties.$createCamp = createCamp;

  // // 获取 作战编成库
  // app.config.globalProperties.$getCamp = getCamp;

  // // 获取 作战编成详情
  // app.config.globalProperties.$getCampDetails = getCampDetails;

  // // 获取 武器库
  // app.config.globalProperties.$getWeapons = getWeapons;

  // // 获取 武器库
  // app.config.globalProperties.$getWeaponDetails = getWeaponDetails;
}
