/**
 * npm: https://www.npmjs.com/package/leaflet
 * https://leafletjs.com/reference.html
 */
import L from 'leaflet';
import LeafletMark from 'leaflet/dist/images/marker-icon-2x.png';

/**
 * npm: https://www.npmjs.com/package/@geoman-io/leaflet-geoman-free
 * API: https://github.com/geoman-io/leaflet-geoman
 */
import '@geoman-io/leaflet-geoman-free';

/**
 * https://www.npmjs.com/package/leaflet.animatedmarker
 * /
 */
import 'leaflet.animatedmarker/src/AnimatedMarker';

/**
 * 创建地图
 *
 */
const DefaultMapConfig = {
  center: [23.035, 121.505], // 初始地图中心
  zoom: 1, // 初始缩放比例
  maxZoom: 17, // 放大比例，数值越大能看的越具体；（实景地图基本是17 实测）
  minZoom: 1, // 缩小比 最小1
  scrollWheelZoom: true, // 滚轮缩放
  zoomControl: false, // 显示缩放控件
  attributionControl: false, // 显示右下角标识
  layers: [
    L.tileLayer(
      'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
      {
        attribution: 'tese <a href="/">link tag</a>', // 有下角标识 添加信息，不建议使用；会有二毛国旗
      },
    ),
  ], // 最初将添加到地图的图层数组
};
export const createMap = (dom, options = DefaultMapConfig) => {
  options = { ...DefaultMapConfig, ...options };
  const maps = L.map(dom, options);
  // const url =
  //   'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}';
  // L.tileLayer(url).addTo(maps);

  return maps;
};

/**
 * 添加图标（棋子）
 * iconUrl null （必需）图标图像的 URL（绝对或相对于您的脚本路径）。
 * iconRetinaUrl null 图标图像的视网膜大小版本的 URL（绝对或相对于您的脚本路径）。用于 Retina 屏幕设备。
 * iconSize null 图标图像的大小（以像素为单位）。
 * iconAnchor null 图标“尖端”的坐标（相对于其左上角）。图标将对齐，以便该点位于标记的地理位置。如果指定了大小，则默认居中，也可以在 CSS 中设置负边距。
 * popupAnchor [0, 0] 弹出窗口将“打开”的点的坐标，相对于图标锚点。
 * tooltipAnchor [0, 0] 工具提示将“打开”的点的坐标，相对于图标锚点。
 * shadowUrl null 图标阴影图像的 URL。如果未指定，则不会创建阴影图像。
 * shadowRetinaUrl null
 * shadowSize null 阴影图像的大小（以像素为单位）。
 * shadowAnchor null 阴影“尖端”的坐标（相对于其左上角）（如果未指定，则与 iconAnchor 相同）。
 * className '' 分配给图标和阴影图像的自定义类名称。默认为空。
 * crossOrigin false 是否将 crossOrigin 属性添加到图块中。如果提供了字符串，则所有图块的 crossOrigin 属性都将设置为提供的字符串。如果您想访问平铺像素数据，则需要这样做。有关有效的字符串值，请参阅CORS 设置。
 */
const DefaultIcon = {
  iconUrl: LeafletMark, // 图标 url
  iconSize: [20, 32], // 图标尺寸 number 像素单位
  // iconAnchor: [0, 0], // 图标偏移量

  shadowUrl: '', // 添加阴影 图片
  shadowSize: [20, 32], // 阴影尺寸
  shadowAnchor: [0, 0], // 阴影偏移量

  popupAnchor: [10, 0], // 气泡偏移量
  popupCont: '', // 气泡文案
};
export const addIcon = (maps, coordinate, options = DefaultIcon) => {
  const { lat, lng } = maps.getCenter();
  options = { ...DefaultIcon, ...options };
  const baseIcon = L.icon(options);
  const Icons = L.marker(coordinate ? coordinate : [lat, lng], {
    icon: baseIcon,
  });
  if (!!options.popupCont) {
    Icons.bindPopup(options.popupCont).addTo(maps);
  } else {
    Icons.addTo(maps);
  }
};

// position	'topleft'	位置	工具栏位置，可能的值为'topleft', 'topright', 'bottomleft','bottomright'
// drawMarker	true	绘图标记	添加按钮以绘制标记。
// drawCircleMarker	true	画圆圈标记	添加按钮以绘制 CircleMarkers。
// drawPolyline	true	绘制折线	添加按钮以绘制线条。
// drawRectangle	true	绘制矩形	添加按钮以绘制矩形。
// drawPolygon	true	绘制多边形	添加按钮以绘制多边形。
// drawCircle	true	画圆	添加按钮以绘制圆形。
// drawText	true	绘制文本	添加按钮以绘制文本。
// editMode	true	编辑模式	添加按钮以切换所有图层的编辑模式。
// dragMode	true	拖动模式	添加按钮以切换所有图层的拖动模式。
// cutPolygon	true	切割多边形	添加按钮以在多边形或线中切割一个孔。
// removalMode	true	移除模式	添加一个按钮以删除图层。
// rotateMode	true	旋转模式	添加一个按钮来旋转图层。
// oneBlock	false	一个块	所有按钮将显示为一个块Customize Controls。
// drawControls	true	绘图控件	显示块中的所有绘制按钮/按钮draw。
// editControls	true	编辑控件	显示块中的所有编辑按钮/按钮edit。
// customControls	true	自定义控件	显示custom块中的所有按钮。
// optionsControls	true	选项控件	option显示块中的所有选项按钮/按钮⭐.
// pinningOption	true	固定选项	添加一个按钮来切换固定选项⭐.
// snappingOption	true	捕捉选项	添加一个按钮来切换捕捉选项⭐.
// splitMode	true	分裂模式	添加一个按钮以切换所有图层的拆分模式⭐.
// scaleMode	true	缩放模式	添加一个按钮以切换所有图层的缩放模式⭐.
export const addControls = (maps) => {
  maps.pm.setLang('zh');
  maps.pm.addControls({
    position: 'topleft',
    // positions: {
    //   draw: 'topright',
    //   edit: 'topleft',
    // },
    drawPolygon: true, // 绘制多边形
    drawMarker: true, //绘制标记点
    drawCircleMarker: true, //绘制圆形标记
    drawPolyline: true, //绘制线条
    drawRectangle: true, //绘制矩形
    drawCircle: true, //绘制圆圈
    editMode: true, //编辑多边形
    dragMode: true, //拖动多边形
    cutPolygon: true, // 添加一个按钮以删除多边形里面的部分内容
    removalMode: true, // 清除多边形
  });
};

// 移除工具栏
export const removeControl = (maps) => {
  maps.pm.removeControls();
};

// 自定义工具栏 菜单
export const CustomControl = (maps) => {
  maps.pm.Toolbar.createCustomControl({
    name: 'testA',
    block: 'custom', // draw:绘制 edit:编辑 custom:自定义 options:选项
    className: 'control-icon leaflet-pm-icon-drag', //设置 className 显示图标之类的
    title: '悬停提示',
    actions: [
      {
        text: 'click me',
        onClick() {
          alert('🙋‍♂️');
        },
      },
    ],
  });
};

// 设置绘制后的线条颜色等
export const setDrawPathStyle = (maps, option: any = null) => {
  if (option) {
    // color: 'orange', 绘制线颜色
    // fillColor: 'green', 填充颜色
    // fillOpacity: 0.4, 填充不透明度
    maps.pm.setPathOptions(option);
  }
};

// 添加动画
export const addMarkAnimate = (maps, options = null) => {
  const line = L.polyline([
    [26.071, 121.571],
    [25.341, 122.031],
    [22.561, 122.401],
    [21.141, 121.331],
    [22.431, 119.141],
    [25.1526, 120.292],
    [26.071, 121.571],
  ]);
  const animatedMarker = L.animatedMarker(line.getLatLngs(), {
    autoStart: false, // 设置 中途停止
    // speedList: [1, 3, 2, 2, 3, 2],
    // distance: 300, // 线分块
    interval: 1000, // 动画时长
    onEnd() {
      // 移动结束后移除
      maps.removeLayer(this);
    },
  });

  animatedMarker.start();
  // 动画中途停止
  // setTimeout(function() {
  //   // Stop the animation
  //    animatedMarker.start();
  // }, 1000);
  // setTimeout(function() {
  //   // Stop the animation
  //   animatedMarker.stop();
  // }, 3000);
  // setTimeout(function() {
  //   // Stop the animation
  //   animatedMarker.start();
  // }, 5000);
  maps.addLayer(animatedMarker);
};
